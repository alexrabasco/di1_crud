<?php 
		require_once "inc/auth.inc.php";
		$con=new PDO("mysql:host=localhost;dbname=world;charset=utf8","root",""); //Conexión a BD

		$mostrar=(isset($_POST['num_registros'])? $_POST['num_registros'] : 10);
		$pagina = (isset($_POST['pagina'])? $_POST['pagina'] : 1);
		$primero = (isset($_POST['primero'])? true : false);
		$ultimo = (isset($_POST['ultimo'])? true : false);
		$siguiente = (isset($_POST['siguiente'])? true : false);
		$anterior = (isset($_POST['anterior'])? true : false);
		$enviar = (isset($_POST['mostrar'])? true : false);

		$sql = "SELECT * from city";

		$id = (isset($_POST['id'])? $_POST['id'] : "");
		$name=(isset($_POST['name'])? $_POST['name'] : "");
		$cc = (isset($_POST['countrycode'])? $_POST['countrycode'] : "");
		$district = (isset($_POST['district'])? $_POST['district'] : "");
		$population = (isset($_POST['population'])? $_POST['population'] : "");
		$tipo = (isset($_POST['valor'])? $_POST['valor'] : "");
		$buscar = (isset($_POST['enviar'])? true : false);

		if (!empty($id) or !empty($name) or !empty($cc) or !empty($district) or !empty($population) or !empty($tipo)) {
			$sql = "SELECT  * FROM city where ";
			$filters= array();
			$arr_where = array();
			if (!empty($filters)) {
				$sql .=" where";
			}

			if (!empty($id)) {

				$arr_where[]="ID=:id";
				$filters[":id"] = $id;
			}
			if (!empty($name)) {
				
				$arr_where[]="Name like :name";
				$filters[":name"] = "%".$name."%";
				
			}
			if (!empty($cc)) {
				$arr_where[]="CountryCode like :countrycode";
				$filters[":countrycode"] = "%".$cc."%";
			}
			if (!empty($district)) {
				$arr_where[]="District like :district";
				$filters[":district"] = "%".$district."%";
			}
			if (!empty($population)) {
				$arr_where[]="Population ".$tipo." :population";
				$filters[":population"] = $population;
			}

				$sql .=implode(" and ", $arr_where);
				$res = $con->prepare($sql);
				$res->execute($filters);
				
				$resultadobuscador = $con->prepare($sql);
				$total=$res->rowCount();
				
				$paginas = ceil($total/$mostrar);
		        if ($primero) $pagina = 1;
		        if ($ultimo) $pagina = $paginas;
		        if ($siguiente && $pagina<$paginas) $pagina++;
		        if ($anterior && $pagina>1) $pagina--;

				if ($mostrar != "todos")
		        	$sql .= " limit ".($mostrar*($pagina-1)) .",". $mostrar;
		        	$statement = $con->prepare($sql);

		        $stmt = $con->prepare($sql);
				$stmt->execute($filters);
				$result = $stmt->fetchAll();

		} else {

	
		$sql="SELECT * FROM city";
		$busquedatotal=$con->query("Select * from city");
		$total=$busquedatotal->rowCount(); //Obtenemos el numero total de filas

		$paginas = ceil($total/$mostrar);
        if ($primero) $pagina = 1;
        if ($ultimo) $pagina = $paginas;
        if ($siguiente && $pagina<$paginas) $pagina++;
        if ($anterior && $pagina>1) $pagina--;
        if ($enviar) $pagina = 1;

		if ($mostrar != "todos")
        	$sql .= " limit ".($mostrar*($pagina-1)) .",". $mostrar;
        	$statement = $con->prepare($sql);
		$statement->execute();
		$result = $statement->fetchAll();

		}




 ?>
<!DOCTYPE html>
<html>
<head>
	<title>Listar</title>
</head>
<body>
	<link rel="stylesheet" type="text/css" href="css/listar.css">
	 <script type="text/javascript">
	 	function confirmDelete(){
	 		var respuesta = confirm("¿Estás seguro que deseas eliminar el usuario?");
	 		if (respuesta==true) {
	 			return true;
	 		} else {
	 			return false;
	 		}
	 	}
	 </script>
	 <div>
	 	<div align="center">
	 			 <a href="inc/form.php">Añadir ciudad</a>
	<table border="2px">
		<thead>
			<tr>
				<th>ID</th>
				<th>Nombre</th>
				<th>Código</th>
				<th>Distrito</th>
				<th>Poblacion</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($result as $row ) : ?>
				
			<tr>
				<td><?php echo ($row['ID']); ?></td>
				<td><?php echo ($row['Name']); ?></td>
				<td><?php echo ($row['CountryCode']); ?></td>
				<td><?php echo ($row['District']); ?></td>
				<td><?php echo ($row['Population']); ?></td>
				<td><a href="inc/editar.php?eid=<?php echo ($row['ID']); ?>">EDITAR</a></td> <!--Pasamos el id para eliminar y editar al pulsar botón-->
				<td><a href="inc/eliminar.php?did=<?php echo ($row['ID']); ?>" onclick="return confirmDelete()">ELIMINAR</a></td>
				<?php endforeach ?>
			</tr>
		</tbody>
	</table>
</div>
    <div>
    	<form action="index.php" method="post">
        <input type="submit" value="<<" name="primero" /> &nbsp;
        <input type="submit" value="<" name="anterior" /> &nbsp;
        <input type="text" name="pagina" value="<?php echo $pagina?>" /> &nbsp;
        <input type="submit" value=">" name="siguiente" /> &nbsp;
        <input type="submit" value=">>" name="ultimo" /> &nbsp;
        <label for="num_registros">Registros por página: </label>
        <select name="num_registros">
            <option value="10" >10</option>
            <option value="15" >15</option>
            <option value="20" >20</option>
            <option value="todos">todos</option>
        </select>
        <input type="submit" value="Mostrar" name="mostrar" />
        <br/><br/>
        <span>Núm. Registros: <?php echo isset($total)?$total:""?></span> &nbsp;
        <span>Página <?php echo isset($paginas)?$pagina ."/". $paginas:""?></span>
        </form>
    </div>
</body>
</html>
