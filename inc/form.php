<?php require_once "auth.inc.php"; ?>
<style type="text/css">
input[type=text], select {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
}

input[type=submit] {
  width: 100%;
  background-color: #71cbf5;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  border-radius: 4px;
  cursor: pointer;
}

input[type=submit]:hover {
  background-color: #45a049;
}

div {
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 20px;
}
</style>
	<!DOCTYPE html>
	<html>
	<head>
		<title>Formulario</title>
		<link rel="stylesheet" type="text/css" href="../css/form.css">
	</head>
	<body>
		<div>
		<form method="post" action="añadir.php">
			<p>ID</p>
			<input type="text" name="ID">
			<p>Nombre</p>
			<input type="text" name="Name">
			<p>CountryCode</p>
			<input type="text" name="CountryCode">
			<p>Distrito</p>
			<input type="text" name="District">
			<p>Población</p>
			<input type="text" name="Population">
			<br>
			<input type="submit" name="submit" value="Añadir">
			</div>
		</form>
	</body>
	</html>